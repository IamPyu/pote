#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include "pote.h"

void pote_init_editor(WINDOW **window, struct editor *editor) {
  *editor = (struct editor){
    .buffer = (struct buffer){
      .text = malloc(BUFFER_SIZE)
    },
    .mx = 0,
    .my = 0
  };
  strncpy(editor->buffer.text, "", BUFFER_SIZE);

  *window = initscr();
  noecho(); raw();
  intrflush(*window, FALSE);
  keypad(*window, TRUE);

  start_color();
  init_pair(WHITE_PAIR, COLOR_BLACK, COLOR_WHITE);
  init_pair(BLACK_PAIR, COLOR_WHITE, COLOR_BLACK);
}

void pote_execute_file(struct editor *editor) {
  char *path = "/tmp/pote-executable-script";
  FILE *tmp = fopen(path, "w");
  fprintf(tmp, "%s", editor->buffer.text);
  fclose(tmp);
  chmod(path, 0700);
  pclose(popen(path, "w"));
  remove(path);
}
