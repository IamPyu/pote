#ifndef POTE_H
#define POTE_H

#include <menu.h>
#include <panel.h>
#include <curses.h>

struct editor {
  struct buffer {
    char *text;
  } buffer;
  int mx, my; // mouse position
  int MAX_ROWS, MAX_COLS;
};

#define BUFFER_SIZE 60000
#define ctrl(x) ((x) & 0x1f)

#define WHITE_PAIR 1
#define BLACK_PAIR 2

void pote_init_editor(WINDOW **window, struct editor *editor);
void pote_execute_file(struct editor *editor);

#endif // POTE_H
