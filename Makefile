include config.mk

CFLAGS += -Wall -Wextra -std=c99
LDFLAGS += -lmenu -lpanel -lncurses 

ifeq ($(DEBUG),1)
	CFLAGS+=-g
endif

SRC = pote.c util.c
OBJS = $(SRC:.c=.o)

.PHONY: all install clean uninstall

all: pote
pote: $(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) $(OBJS) -o pote

%.o: %.c
	$(CC) -c $(CFLAGS) $(LDFLAGS) $< -o $@

clean:
	rm -rf $(OBJS)
	rm -rf pote

install: pote
	cp pote $(PREFIX)/bin

uninstall:
	rm -rf $(PREFIX)/bin/pote
