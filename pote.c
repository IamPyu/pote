#define _GNU_SOURCE
#include <locale.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <getopt.h>
#include <sys/stat.h>

#include "pote.h"

WINDOW *window;
struct editor editor;

#define move_curs(e) e; //wmove(window, editor.my, editor.mx)

int main(int argc, char *argv[]) {
  setlocale(LC_ALL, "en_US.UTF-8");

  editor = (struct editor){0};
  pote_init_editor(&window, &editor);
  
  int och;
  bool help = false;

  while ((och = getopt(argc, argv, "h")) != -1) {
    switch (och) {
    case 'h':
    default:
      help = true;
      goto done;
    }
  }
  argc -= optind;
  argv += optind;

  char *filename;
  
  if (argv[0] != NULL) {
    filename = argv[0];
  } else {
    filename = "file.txt";
  }
  
  bool file_exists = true;
  {
    FILE *reading_file = fopen(filename, "r");

    if (reading_file == NULL) {
      strncpy(editor.buffer.text, "POTE EDITOR", BUFFER_SIZE);
      file_exists = false;
      goto out;
    }
    
    char *buffer = NULL;
    size_t len = 0;
    ssize_t nread;
    
    while ((nread = getline(&buffer, &len, reading_file)) != -1) {
      strncat(editor.buffer.text, buffer, BUFFER_SIZE);
    }
    
    free(buffer);
    fclose(reading_file);
  out:
    wprintw(window, "%s", editor.buffer.text);
  }
  const char *original_text = strndup(editor.buffer.text, BUFFER_SIZE);
  
  
  FILE *file = fopen(filename, "w");
  if (file == NULL) {
    wprintw(window, "Failed to open file! Closing in 3 seconds.");
    sleep(3);
    goto done;
  }

  int ch;
  bool exitp, edited = false;
  for (;;) {
    getmaxyx(window, editor.MAX_COLS, editor.MAX_ROWS);
    getyx(window, editor.my, editor.mx);
    
    attron(COLOR_PAIR(WHITE_PAIR) | A_BOLD | A_UNDERLINE);
    mvwprintw(window, editor.MAX_COLS - 1, 0, "[File: %s] [Rows: %d;Cols: %d] [Keybinds: C-s: Save, C-q: Quit, C-e: Execute file]", filename, editor.mx, editor.my);
    attroff(COLOR_PAIR(WHITE_PAIR) | A_BOLD | A_UNDERLINE);

    wmove(window, editor.my, editor.mx);

    ch = wgetch(window);
    
    // main keys
    switch (ch) {
    case ctrl('q'):
      exitp = true;
      break;
    case ctrl('s'):
      edited = true;
      fprintf(file, "%s", editor.buffer.text);
      break;
    case ctrl('e'):
      pote_execute_file(&editor);
      break;
    case KEY_BACKSPACE:
      editor.buffer.text[strnlen(editor.buffer.text, BUFFER_SIZE)-1] = '\0';
      break;
    case KEY_ENTER:
      strncat(editor.buffer.text, (char[]){'\n', '\0'}, BUFFER_SIZE);
      break;
    case KEY_RESIZE:
      break;
    case ERR:
      break;
    default:
      strncat(editor.buffer.text, (char[]){ch, '\0'}, BUFFER_SIZE);
      break;
    }
    if (exitp) break;

    wclear(window);
    wprintw(window, "%s", editor.buffer.text);
    wrefresh(window);
  }

  if (!edited && file_exists) {
    fprintf(file, "%s", original_text);
  }
  
  fclose(file);

  if (!edited && !file_exists) {
    remove(filename);
  }
 done:
  endwin();
  printf("Goodbye, %s!\n", getenv("USER"));
  
  if (help) {
    fprintf(stderr, "Usage: %s [-h] [-f arg]\n", argv[0]);
    return 1;
  }
  
  return 0;
}
